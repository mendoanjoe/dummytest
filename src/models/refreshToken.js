const mongoose = require('mongoose');
const moment = require('moment-timezone');
const crypto = require('crypto');

/**
 * refreshToken schema
 * @private
 */
const refreshTokenSchema = new mongoose.Schema({
  token: {
    type: mongoose.Schema.Types.String,
    required: true,
    index: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'users',
  },
  email: {
    type: mongoose.Schema.Types.String,
    ref: 'users',
  },
  accessToken: {
    type: mongoose.Schema.Types.String,
    required: true,
    index: true,
  },
  expires: { type: mongoose.Schema.Types.String },
},
{
  timestamps: true,
  collection: 'refreshToken',
});

refreshTokenSchema.statics = {
  async generate(userId, userEmail, reqAccessToken, client) {
    const token = `${userId}.${crypto.randomBytes(40).toString('hex')}`;
    const expires = moment().add(client.expiration, 'days').toDate();
    const accessToken = `Bearer ${reqAccessToken}`;

    await RefreshToken.deleteMany({ userId });
    const tokenObject = new RefreshToken({
      token, userId, userEmail, accessToken, expires,
    });
    tokenObject.save();

    return tokenObject;
  },

};

/**
 * @typedef refreshToken
 */
const RefreshToken = mongoose.model('refreshToken', refreshTokenSchema);
module.exports = RefreshToken;
