const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const moment = require('moment-timezone');
const jwt = require('jsonwebtoken');
const Rest = require('rest-errors');
const { errorMessage, traceError } = require('../utils/Helper');
const { env, jwtSecret } = require('../config/vars');
const { ADMIN_USER, SUPER_ADMIN } = require('../config/permission');

const restErrors = new Rest({ lang: 'id' });

/**
 * users schema
 * @private
 */
const userSchema = new mongoose.Schema({
  email: {
    type: mongoose.Schema.Types.String,
    unique: true,
    trim: true,
    lowercase: true,
  },
  password: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  name: {
    type: mongoose.Schema.Types.String,
    required: true,
    trim: true,
  },
  salary: {
    type: mongoose.Schema.Types.String,
    required: true,
    trim: true,
    default: 0,
  },
  age: {
    type: mongoose.Schema.Types.String,
    required: true,
    trim: true,
    default: 0,
  },
  role: {
    type: [mongoose.Schema.Types.String],
    trim: true,
  },
},
{
  timestamps: true,
  collection: 'users',
});

async function compareItem(data, body) {
  let base = data;
  const {
    name, password, email, salary, age,
  } = body;

  base = name ? { ...base, name } : base;
  base = password ? { ...base, password } : base;
  base = email ? { ...base, email } : base;
  base = salary ? { ...base, salary } : base;
  base = age ? { ...base, age } : base;

  return base;
}

/**
 * before save will run this script
 */
userSchema.pre('save', async function save(next) {
  try {
    if (!this.isModified('password')) return next();
    const rounds = env === 'test' ? 1 : 10;
    const hash = await bcrypt.hash(this.password, rounds);
    this.password = hash;
    return next();
  } catch (error) {
    return next(error);
  }
});

userSchema.method({
  async transform() {
    const t = {};
    const fields = ['id', 'email', 'name', 'age', 'salary'];

    fields.map((field) => {
      t[field] = this[field];
      return t[field];
    });

    return t;
  },

  token() {
    const playload = {
      exp: moment().add(30, 'days').unix(),
      iat: moment().unix(),
      sub: this._id,
    };
    return jwt.sign(playload, jwtSecret);
  },

  async passwordMatches(password) {
    return bcrypt.compare(password, this.password);
  },

  async compare(body) {
    const data = await compareItem(this, body);
    return data;
  },
});

userSchema.statics = {
  async compare(data, body) {
    const ns = compareItem(data, body);
    return ns;
  },

  async findAndGenerateToken(options, client) {
    const { email, password, refreshObject } = options;

    const { message } = restErrors.invalid();
    const user = await this.findOne({ email }).exec();

    const err = {
      status: 401,
      isPublic: true,
    };

    if (password) {
      if (user && await user.passwordMatches(password)) {
        return { user, accessToken: user.token(client) };
      }

      err.message = message;
    } else if (refreshObject && refreshObject.userEmail === email) {
      if (moment(refreshObject.expires).isBefore()) err.message = message;
      return { user, accessToken: user.token() };
    }

    return errorMessage({ message: err.message, status: 401 });
  },

  async checkDuplicateEmail(email) {
    try {
      const user = await this.find({ email });
      if (user.length > 0) {
        return errorMessage(restErrors.accountRegisteredEmail());
      }

      return true;
    } catch (error) {
      return traceError(error);
    }
  },

  async findOneAndUpdateT(userId, body, cb) {
    let user = await this.findOneAndUpdate({ _id: userId }, body, cb);
    user = await this.findById(userId, cb);
    user = await user.transform();

    return user;
  },

  isAdmin({ role }) {
    let a = 0;
    if (role.includes(ADMIN_USER)) a += 1;
    if (role.includes(SUPER_ADMIN)) a += 1;
    if (a > 0) return true;
    return false;
  },
};

/**
 * @typedef users
 */
module.exports = mongoose.model('users', userSchema);
