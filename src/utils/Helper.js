const moment = require('moment-timezone');
const RefreshToken = require('../models/refreshToken');
const APIError = require('./APIError');

exports.errorMessage = (data) => {
  throw new APIError(data);
};

exports.traceError = (error) => {
  if (error.response) {
    throw new APIError({
      message: error.response.data.message,
      status: 400,
    });
  } else throw error;
};

exports.generateToken = (user, accessToken, client) => {
  const tokenType = 'Bearer';
  const refreshToken = RefreshToken.generate(user.id, user.email, accessToken, client).token;
  const expiresIn = moment().add(client.expiration, 'days');
  return {
    tokenType, accessToken, refreshToken, expiresIn,
  };
};
