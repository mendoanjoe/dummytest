const passport = require('passport');
const Rest = require('rest-errors');
const APIError = require('../utils/APIError');

const restErrors = new Rest({ lang: 'id' });

const handleJWT = (req, res, next) => async (err, user, info) => {
  const error = err || info;
  const logIn = Promise.promisify(req.logIn);

  const apiError = new APIError({
    message: error ? error.message : restErrors.unauthorized().message,
    status: 401,
    stack: error ? error.stack : undefined,
  });

  try {
    if (error) throw error;
    if (!user) throw error;
    await logIn(user, { session: false });
  } catch (e) {
    return next(apiError);
  }

  /**
   * Save user data in memory (like redis)
   * @variable req.user
   */
  req.user = user;
  return next();
};

exports.authorize = () => (req, res, next) => {
  passport.authenticate('jwt', { session: false }, handleJWT(req, res, next))(req, res, next);
};

exports.checkPermission = roles => async (req, res, next) => {
  let authorize = 0;

  roles.map(async (data) => {
    if (req.user) if (req.user.role.includes(data) === true) authorize += 1;
  });

  if (authorize === 0) {
    const apiError = new APIError({
      message: restErrors.unauthorized().message,
      status: 401,
      stack: undefined,
    });
    return next(apiError);
  }
  return next();
};
