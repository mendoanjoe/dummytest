const expressValidation = require('express-validation');
const Rest = require('rest-errors');
const APIError = require('../utils/APIError');
const { errorMessage } = require('../utils/Helper');
const { env } = require('../config/vars');
const logger = require('../config/logger');

const restErrors = new Rest({ lang: 'id' });

/**
 * Error handler. Send stacktrace only during development
 * @public
 */
const handler = (err, req, res, next) => {
  const response = {
    status: err.status,
    message: err.message || err.status,
    errors: err.errors,
    stack: err.stack,
  };

  if (env !== 'development') {
    delete response.stack;
    delete response.errors;
  }

  res.status(err.status);
  res.json(response);
  res.end();
};
exports.handler = handler;

/**
 * If error is not an instanceOf APIError, convert it.
 * @public
 */
exports.converter = (err, req, res, next) => {
  let convertedError = err;

  if (err instanceof expressValidation.ValidationError) {
    convertedError = new APIError({
      message: restErrors.invalid().message,
      errors: err.errors,
      status: err.status,
      stack: err.stack,
    });
  } else if (!(err instanceof APIError)) {
    convertedError = new APIError({
      message: err.message,
      status: err.status,
      stack: err.stack,
    });
  }

  return handler(convertedError, req, res);
};

/**
 * Catch 404 and forward to error handler
 * @public
 */
exports.notFound = (req, res, next) => {
  logger.error(`Not Found - ${req.method} ${req.originalUrl}`);
  const err = errorMessage(restErrors.notFound());
  return handler(err, req, res);
};
