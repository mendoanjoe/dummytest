const express = require('express');
const validate = require('express-validation');
const controller = require('./user.controller');
const { update } = require('./user.validation');
const { authorize, checkPermission } = require('../../middlewares/auth');
const { ADMIN_USER, SUPER_ADMIN } = require('../../config/permission');

const router = express.Router();

router.route('/me').get(authorize(), controller.me);
router.route('/list').get(authorize(), checkPermission([ADMIN_USER, SUPER_ADMIN]), controller.list);
router.route('/detail/:userId')
  .get(authorize(), controller.getById)
  .put(authorize(), validate(update), controller.update)
  .delete(authorize(), controller.delete);

module.exports = router;
