const Rest = require('rest-errors');
const User = require('../../models/user');
const { traceError, errorMessage } = require('../../utils/Helper');

const restErrors = new Rest({ lang: 'id' });

exports.me = async (req) => {
  try {
    return req.user.transform();
  } catch (error) {
    return traceError(error);
  }
};

exports.list = async () => {
  try {
    const user = await User.find();
    return user;
  } catch (error) {
    return traceError(error);
  }
};

exports.getById = async (req) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) return errorMessage(restErrors.notFound);

    return user.transform();
  } catch (error) {
    return traceError(error);
  }
};

exports.update = async (req) => {
  try {
    let user = await User.findById(req.params.userId);
    if (!user) errorMessage(restErrors.notFound());
    if (req.user.id !== user.id) errorMessage(restErrors.authError);

    if (req.body.password) {
      if (!user.passwordMatches(req.body.password)) errorMessage(restErrors.invalid());
    }

    let updateBody = {};
    updateBody = await User.compare(updateBody, req.body);
    user = await User.findOneAndUpdateT(req.params.userId, updateBody);

    return user;
  } catch (error) {
    return traceError(error);
  }
};

exports.delete = async (req) => {
  try {
    const user = await User.findByIdAndDelete(req.params.userId);
    if (!user) return errorMessage(restErrors.notFound);

    return true;
  } catch (error) {
    return traceError(error);
  }
};
