const Joi = require('joi');

module.exports = {
  login: {
    body: {
      email: Joi.string().email(),
      password: Joi.string().required(),
    },
  },
  update: {
    body: {
      name: Joi.string(),
      age: Joi.string(),
      salary: Joi.string(),
    },
  },
};
