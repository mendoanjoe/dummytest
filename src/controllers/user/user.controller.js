const service = require('./user.service');

exports.me = async (req, res, next) => {
  try {
    const data = await service.me(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};

exports.list = async (req, res, next) => {
  try {
    const data = await service.list(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};

exports.getById = async (req, res, next) => {
  try {
    const data = await service.getById(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};

exports.update = async (req, res, next) => {
  try {
    const data = await service.update(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const data = await service.delete(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};
