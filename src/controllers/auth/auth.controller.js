const service = require('./auth.service');

exports.login = async (req, res, next) => {
  try {
    const data = await service.login(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};

exports.register = async (req, res, next) => {
  try {
    const data = await service.register(req);

    const response = {
      status: 200,
      message: 'OK.',
      data,
    };

    return res.status(200).json(response);
  } catch (error) {
    return next(error);
  }
};
