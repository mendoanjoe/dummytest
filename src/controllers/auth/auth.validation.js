const Joi = require('joi');

module.exports = {
  login: {
    body: {
      email: Joi.string().email(),
      password: Joi.string().required(),
    },
  },
  register: {
    body: {
      name: Joi.string().required(),
      age: Joi.string().required(),
      salary: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string().email().required(),
    },
  },
};
