const express = require('express');
const validate = require('express-validation');
const controller = require('./auth.controller');
const { login, register } = require('./auth.validation');

const router = express.Router();

router.route('/login')
  .post(validate(login), controller.login);

router.route('/register')
  .post(validate(register), controller.register);

module.exports = router;
