const Rest = require('rest-errors');
const User = require('../../models/user');
const { WEB_USER } = require('../../config/permission');
const { traceError, errorMessage, generateToken } = require('../../utils/Helper');

const restErrors = new Rest({ lang: 'id' });

exports.login = async (req) => {
  try {
    if (!req.client) errorMessage(restErrors.unauthorized());
    const { user, accessToken } = await User.findAndGenerateToken(req.body, req.client);

    const token = generateToken(user, accessToken, req.client);
    const userTransformed = await user.transform();

    return { token, user: userTransformed };
  } catch (error) {
    return traceError(error);
  }
};

exports.register = async (req) => {
  try {
    await User.checkDuplicateEmail(req.body.email);

    let user = await User.compare({}, req.body);
    user = new User(user);
    user.role = [WEB_USER];
    await user.save();

    return user.transform();
  } catch (error) {
    return traceError(error);
  }
};
