Promise = require('bluebird'); // eslint-disable-line no-global-assign

const { port, env } = require('./config/vars');
const logger = require('./config/logger');
const app = require('./config/express');
const db = require('./config/db');

// create database connection
db.connect();

app.listen(port || process.env.PORT, () => logger.info(`server started on port ${port} (${env})`));

/**
* Exports express
* @public
*/
module.exports = app;
