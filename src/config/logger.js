/**
 * Log with create two file
 * with different level
 * @error ,@info
 */
const winston = require('winston');
const moment = require('moment-timezone');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({ format: winston.format.simple() }));
}

logger.stream = {
  write: (message) => {
    const remessage = `${message} at ${moment().toString()}`;
    logger.info(remessage.trim());
  },
};

module.exports = logger;
