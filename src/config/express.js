const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compress = require('compression');
const helmet = require('helmet');
const passport = require('passport');
const { logs } = require('./vars');
const strategies = require('./passport');
const routes = require('../routes/v1');
const error = require('../middlewares/error');

/**
* express instance
* @public
*/
const app = express();

app.use(morgan(logs));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compress());
app.use(helmet());

/**
 * express authenication
 * [jwt]
 */
app.use(passport.initialize());
app.use(passport.session());
passport.use('jwt', strategies.jwt);

/**
 * express routes
 */
app.use('/v1', routes);

/**
 * express error handler
 */
app.use(error.converter);
app.use(error.notFound);
app.use(error.handler);

module.exports = app;
