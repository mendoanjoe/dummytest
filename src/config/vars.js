const path = require('path');
require('dotenv-safe').load({
  path: path.join(__dirname, '../../.env'),
  sample: path.join(__dirname, '../../.env.example'),
});

let databaseUrl = process.env.NODE_ENV === 'development' ? process.env.MONGO_URI_DEV : process.env.MONGO_URI;
databaseUrl = process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TEST : databaseUrl;

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  host: process.env.HOST,

  jwtSecret: process.env.JWT_SECRET,
  jwtExpirationInterval: process.env.JWT_EXPIRATION_MINUTES,

  mongo: { uri: databaseUrl },
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
};
