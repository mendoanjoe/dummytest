const express = require('express');
const authRoutes = require('../../controllers/auth/auth.route');
const userRoutes = require('../../controllers/user/user.route');

const router = express.Router();

/**
 * Return HTTP_STATUS
 * @public
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * Return sub-router
 * @public
 */
router.use('/auth', authRoutes);
router.use('/user', userRoutes);

module.exports = router;
